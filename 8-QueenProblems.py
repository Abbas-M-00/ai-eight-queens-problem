import math
import random

def simulatedAnnealing(T, board = [0, 0, 0, 0, 0, 0, 0, 0]) :
    t = T

    while t > 0:
        e1 = evaluateFunction(board)

        if e1 == 0:
            print("\n\n T => ", t)
            return

        columnNumber = math.ceil(random.random() * 8) - 1
        randomValue = math.ceil(random.random() * 8) - 1

        tmpBoard = board[:]
        tmpBoard[columnNumber] = randomValue

        e2 = evaluateFunction(tmpBoard)
        deltaE = e2 - e1

        print("E1 = ", e1)
        print("E2 = ", e2)
        print("deltaE = ", deltaE)
        print("T = ", t)

        if deltaE <= 0 :
            board[columnNumber] = randomValue
        else :
            chance = math.exp(-1 * deltaE / t)
            randomChance = random.random()

            print("chance = ", chance)
            print("randomChance = ", randomChance)

            if randomChance < chance :
                print("-> Do a bad move! <-")
                board[columnNumber] = randomValue
                t -= 1

        print(board)
        print("-------------------------------------\n")


def evaluateFunction(board):
    e = 0
    for i in range(len(board) - 1) :
        for j in range(i + 1, len(board)) :
            if board[j] == board[i] or board[j] == board[i] + (j - i) or board[j] + (j - i) == board[i] :
                e += 1
    return e

simulatedAnnealing(20)
